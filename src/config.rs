// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
use std::collections::HashMap;

use nvim_oxi::{self as oxi, conversion::FromObject, serde::Deserializer};
use oxi::{conversion::ToObject, lua, serde::Serializer, Object};
use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct BuildStyle {
    pub cmd: String,
    pub build_flags: Vec<String>,
    pub run_flags: Vec<String>,
    pub file: String,
}

impl FromObject for BuildStyle {
    fn from_object(object: Object) -> Result<Self, oxi::conversion::Error> {
        Self::deserialize(Deserializer::new(object)).map_err(Into::into)
    }
}

impl ToObject for BuildStyle {
    fn to_object(self) -> Result<Object, oxi::conversion::Error> {
        self.serialize(oxi::serde::Serializer::new())
            .map_err(Into::into)
    }
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Config {
    pub build_styles: HashMap<String, BuildStyle>,
}

impl Config {
    pub fn get_build_style_from_file_name(&self, file: impl Into<String>) -> Option<BuildStyle> {
        let file: String = file.into();
        for (_, v) in self.build_styles.iter() {
            if v.file == file {
                return Some(v.clone());
            }
        }
        None
    }
}

impl FromObject for Config {
    fn from_object(object: Object) -> Result<Self, oxi::conversion::Error> {
        Self::deserialize(Deserializer::new(object)).map_err(Into::into)
    }
}

impl ToObject for Config {
    fn to_object(self) -> Result<Object, oxi::conversion::Error> {
        self.serialize(Serializer::new()).map_err(Into::into)
    }
}

impl lua::Poppable for Config {
    unsafe fn pop(lstate: *mut lua::ffi::lua_State) -> Result<Self, lua::Error> {
        let obj = Object::pop(lstate)?;
        Self::from_object(obj).map_err(lua::Error::pop_error_from_err::<Self, _>)
    }
}

impl lua::Pushable for Config {
    unsafe fn push(self, lstate: *mut lua::ffi::lua_State) -> Result<std::ffi::c_int, lua::Error> {
        self.to_object()
            .map_err(lua::Error::pop_error_from_err::<Self, _>)?
            .push(lstate)
    }
}

impl Default for Config {
    fn default() -> Self {
        let build_styles = HashMap::from([
            (
                "cargo".into(),
                BuildStyle {
                    cmd: "cargo".into(),
                    build_flags: vec!["build".into()],
                    run_flags: vec!["run".into()],
                    file: "Cargo.toml".into(),
                },
            ),
            (
                "make".into(),
                BuildStyle {
                    cmd: "make".into(),
                    build_flags: Vec::new(),
                    run_flags: vec!["run".into()],
                    file: "Makefile".into(),
                },
            ),
            (
                "just".into(),
                BuildStyle {
                    cmd: "just".into(),
                    build_flags: Vec::new(),
                    run_flags: vec!["run".into()],
                    file: "justfile".into(),
                },
            ),
            (
                "buildsh".into(),
                BuildStyle {
                    cmd: "./build.sh".into(),
                    build_flags: Vec::new(),
                    run_flags: vec!["run".into()],
                    file: "build.sh".into(),
                },
            ),
            (
                "go".into(),
                BuildStyle {
                    cmd: "go".into(),
                    build_flags: vec!["build".into()],
                    run_flags: Vec::new(),
                    file: "go.mod".into(),
                },
            ),
        ]);

        Self { build_styles }
    }
}
