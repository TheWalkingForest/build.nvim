// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

mod config;

use std::{
    fmt::Display,
    fs,
    io::{Read, Write},
    path::{self, PathBuf},
    process::Command,
};

use config::{BuildStyle, Config};
use nvim_oxi as oxi;
use oxi::{api, conversion::ToObject, Dictionary, Function, Object};
use serde::{Deserialize, Serialize};

const BUILD_CONFIG_FILE: &str = ".build-nvim.ron";

fn to_nvim_error<T>(err: T) -> oxi::Error
where
    T: Display,
{
    oxi::Error::Lua(oxi::lua::Error::RuntimeError(format!("{err}")))
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct BuildError {
    desc: String,
}

impl Display for BuildError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "BuildError: {}", self.desc)
    }
}

impl std::error::Error for BuildError {
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        None
    }

    fn description(&self) -> &str {
        "description() is deprecated; use Display"
    }

    fn cause(&self) -> Option<&dyn std::error::Error> {
        self.source()
    }
}

impl From<&str> for BuildError {
    fn from(value: &str) -> Self {
        Self {
            desc: value.to_string(),
        }
    }
}

fn get_files(path: PathBuf) -> Result<Vec<String>, Box<dyn std::error::Error>> {
    let result = fs::read_dir(path)?
        .filter_map(|res| res.map(|e| e.path()).ok())
        .filter_map(|e| e.file_name().map(|f| f.to_owned()))
        .map(|e| e.to_string_lossy().to_string())
        .collect::<Vec<_>>();
    Ok(result)
}

#[allow(dead_code)]
fn pick_style(_styles: &[String]) -> oxi::Result<String> {
    todo!()
}

fn setup_build(_: ()) -> oxi::Result<()> {
    // TODO: Load existing config before setup
    if path::Path::new(BUILD_CONFIG_FILE).exists() {
        return Ok(());
    }

    let path = std::env::current_dir().map_err(to_nvim_error)?;

    let files = get_files(path).map_err(to_nvim_error)?;

    let files = files
        .into_iter()
        .filter(|f| {
            for (_k, v) in Config::default().build_styles.iter() {
                if v.file == **f {
                    return true;
                }
            }
            false
        })
        .collect::<Vec<String>>();

    let style_file = match files.len() {
        0 => return Ok(()),
        1 => files[0].clone(),
        _ => {
            if files.contains(&"Makefile".to_string()) {
                "Makefile".into()
            } else if files.contains(&"justfile".to_string()) {
                "justfile".into()
            } else {
                let mut out = "".to_string();
                for i in files.into_iter() {
                    if i != "Makefile" && i != "justfile" {
                        out = i;
                    }
                }
                out.to_string()
            }
        }
    };

    let mut build_conf_file = fs::OpenOptions::new()
        .create(true)
        .write(true)
        .read(false)
        .open(BUILD_CONFIG_FILE)
        .map_err(to_nvim_error)?;

    let style = Config::default()
        .get_build_style_from_file_name(style_file)
        .ok_or(to_nvim_error("Failed to get build style"))?;

    build_conf_file
        .write_all(
            ron::ser::to_string_pretty(&style, ron::ser::PrettyConfig::default())
                .map_err(to_nvim_error)?
                .as_bytes(),
        )
        .map_err(to_nvim_error)
}

fn run_cmd(cmd: String, args: &[String]) -> oxi::Result<(String, String)> {
    let output = Command::new(cmd)
        .args(args)
        .output()
        .map_err(to_nvim_error)?;
    let std_err = output
        .stderr
        .into_iter()
        .map(|e| e as char)
        .collect::<String>();
    let std_out = output
        .stdout
        .into_iter()
        .map(|e| e as char)
        .collect::<String>();

    Ok((std_out, std_err))
}

fn get_build_style() -> oxi::Result<BuildStyle> {
    let mut build_conf_file = fs::OpenOptions::new()
        .create(false)
        .write(false)
        .read(true)
        .open(BUILD_CONFIG_FILE)
        .map_err(to_nvim_error)?;

    let mut buf = String::new();
    let _bytes_read = build_conf_file
        .read_to_string(&mut buf)
        .map_err(to_nvim_error)?;

    ron::from_str::<BuildStyle>(&buf).map_err(to_nvim_error)
}

fn run(_config: Config) -> oxi::Result<String> {
    let style = get_build_style()?;
    let (std_out, std_err) = run_cmd(style.cmd, &style.run_flags)?;

    let curr_dirr = std::env::current_dir().map_err(to_nvim_error)?;

    let mut result = "".into();
    if !std_out.is_empty() {
        result = format!(
            "{result}===== stdout ({}) =====\n{std_out}\n\n",
            curr_dirr.to_string_lossy()
        );
    }
    if !std_err.is_empty() {
        result = format!(
            "===== stderr ({}) =====\n{std_err}",
            curr_dirr.to_string_lossy()
        );
    }
    Ok(result)
}

fn run_term(_config: Config) -> oxi::Result<()> {
    let mut style = get_build_style()?;
    let mut cmd = vec!["split".to_string(), "term://".to_string(), style.cmd];
    cmd.append(&mut style.build_flags);
    api::command(&cmd.join(" ")).map_err(to_nvim_error)
}

fn build(_config: Config) -> oxi::Result<String> {
    let style = get_build_style()?;
    let (std_out, std_err) = run_cmd(style.cmd, &style.build_flags)?;

    let curr_dirr = std::env::current_dir().map_err(to_nvim_error)?;

    let mut result = "".into();
    if !std_out.is_empty() {
        result = format!(
            "{result}===== stdout ({}) =====\n{std_out}\n\n",
            curr_dirr.to_string_lossy()
        );
    }
    if !std_err.is_empty() {
        result = format!(
            "===== stderr ({}) =====\n{std_err}",
            curr_dirr.to_string_lossy()
        );
    }
    Ok(result)
}

fn build_term(_config: Config) -> oxi::Result<()> {
    let mut style = get_build_style()?;
    let mut cmd = vec!["split".to_string(), "term://".to_string(), style.cmd];
    cmd.append(&mut style.build_flags);
    api::command(&cmd.join(" ")).map_err(to_nvim_error)
}

fn setup(_config: Config) -> oxi::Result<()> {
    Ok(())
}

#[oxi::module]
fn build_nvim() -> oxi::Result<Dictionary> {
    let setup_fn = Function::from_fn(|_: ()| setup(Config::default()));
    let options = Config::default();
    let setup_build_fn = Function::from_fn(setup_build);
    let build_fn = Function::from_fn(|_: ()| build(Config::default()));
    let build_term_fn = Function::from_fn(|_: ()| build_term(Config::default()));
    let run_fn = Function::from_fn(|_: ()| run(Config::default()));
    let run_term_fn = Function::from_fn(|_: ()| run_term(Config::default()));

    Ok(Dictionary::from_iter([
        ("setup", Object::from(setup_fn)),
        ("options", options.to_object()?),
        ("setup_build", Object::from(setup_build_fn)),
        ("build", Object::from(build_fn)),
        ("build_term", Object::from(build_term_fn)),
        ("run", Object::from(run_fn)),
        ("run_term", Object::from(run_term_fn)),
    ]))
}
