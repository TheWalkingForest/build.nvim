# Build.nvim

A simple neovim plugin to detect what kind of build system should be used to build a project
and make it easy to build the project without leaving neovim

## Install

- [Lazy.nvim](https://github.com/folke/lazy.nvim)

```lua
{
    url = 'https://gitlab.com/TheWalkingForest/build.nvim',
    build = {
        'cargo build --release && mkdir lua && mv target/release/libbuild_nvim.so lua/build_nvim.so',
    },
    config = function()
        require('build_nvim').setup()
    end
}
```

## Configuration (TODO)

## TODOs

- [ ] Select prefered way to build/run project if multiple build methods found
  - [x] Add priority to `make` and `just` build styles
- [ ] Make a better way to build and install plugin
- [ ] Add way to configure plugin
- [ ] Add way to edit build and run commands for a project
- [ ] Potentially integrate with existing neovim `makeprg` and `make` commands
- [x] Add option to run command in split or seperate buffer

## License

This respository is licensed under the [MPL 2.0 license](https://www.mozilla.org/en-US/MPL/2.0/) as specified

