" Title:        Build.nvim
" Description:  A plugin to provide an example for creating Neovim plugins.
" Last Change:  25 January 2024
" Maintainer:   TheWalkingForest User <https://gitlab.com/TheWalkingForest>
" License:      This Source Code Form is subject to the terms of the Mozilla Public
"               License, v. 2.0. If a copy of the MPL was not distributed with this
"               file, You can obtain one at https://mozilla.org/MPL/2.0/.

" Prevents the plugin from being loaded multiple times.
if exists("g:loaded_exampleplugin")
    finish
endif
let g:loaded_exampleplugin = 1

augroup BuildAutocmd
    autocmd!
    au VimEnter * lua require("build_nvim").setup_build()
augroup END

" Exposes the plugin's functions for use as commands in Neovim.
command! -nargs=0 BBuild lua require("build_nvim").build()
command! -nargs=0 BRun lua require("build_nvim").run()
command! -nargs=0 BBuildTerm lua require("build_nvim").build_term()
command! -nargs=0 BRunTerm lua require("build_nvim").run_term()
command! -nargs=0 BSetupBuild lua require("build_nvim").setup_build()
